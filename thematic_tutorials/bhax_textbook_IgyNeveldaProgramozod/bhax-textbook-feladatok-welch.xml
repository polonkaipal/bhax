<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Welch!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Első osztályom</title>
        <para>
            Valósítsd meg C++-ban és Java-ban az módosított polártranszformációs algoritmust! A matek háttér 
            teljesen irreleváns, csak annyiban érdekes, hogy az algoritmus egy számítása során két normálist
            számol ki, az egyiket elspájzolod és egy további logikai taggal az osztályban jelzed, hogy van vagy
            nincs eltéve kiszámolt szám.
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>                
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... térj ki arra is, hogy a JDK forrásaiban a Sun programozói
            pont úgy csinálták meg ahogyan te is, azaz az OO nemhogy nem nehéz, hanem éppen természetes neked!
        </para> 
        <para>
            Mielőtt rátérnék a feladatok megoldására, szeretnék egy kis bevezetőt írni az OOP alapelveiről, illetve egy átfogó képet adni arról, mi is az, illetve miért hasznos egy 
            programozónak.
        </para>   
        <para>
            A magas szintű programozási nyelvek legnagyobb előnye az alacsonyabb szintűekkel szemben, hogy lehetőséget adnak az objektum orientál programozásra. Ezzel lényegesen kevesebb 
            munkával sokkal komplexebb, hatékonyabb és átláthatóbb programot tudunk létrehozni. Az OOP már nem a műveleteket helyezi a középpontba, hanem az adatokat, illetve a köztük lévő 
            kapcsolatokat (hierarchiát). 
        </para>
        <para>
            Osztályokat hozunk létre, amikben eltárolhatjuk egy egyed (entitás) tulajdonságait és működését különféle adatok és műveletek segítségével. Ebből az osztályból aztán létre kell 
            hoznunk legalább egy példányt (objektumot). Ezt a folyamatot példányosításnak nevezzük. A tulajdonságokat (attribútumokat) tároló változókat adattagoknak, a műveleteket 
            metódusoknak nevezzük. 
        </para>
        <para>
            Az OOP alapelvei közé tartozik, hogy a felhasználó csak annyi adathoz férjen hozzá, amennyihez feltétlen szükséges. Veszélyes lenne, ha mindenkinek látható és módosítható (!) 
            lenne minden adat. Alapvetően 3 féle láthatóság létezik: public, private és protected. A Public egyértelmű, mindenki számára látható. A Private csak az osztályon belül látható, a 
            származtatott osztályok számára nem látható és nem is módosítható. A Protected majdnem ugyanaz, mint a Private annyi különbséggel, hogy itt már a leszármazott osztályok is 
            hozzáférnek az adatokhoz. C++ban alapértelmezetten minden private. 
        </para>
        <para>
            Fontos fogalom még az enkapszuláció, vagy más néven egységbe zárás. Ez is egyfajta védelmet biztosít, lényegében azt takarja, hogy egy osztály belső szerkezetét gond nélkül 
            megváltoztathatjuk, feltéve, hogy a metódusok ne változzanak. (pl.: mindegy, hogy milyen repülőről beszélünk, legyen az egy utasszállító, vagy egy vadászgép, mindegyikkel tudunk 
            repülni. Vagy teljesen mindegy, hogy milyen autóról beszélünk, mindegyiknek van kormánya, amivel irányítani tudjuk azt.)
        </para>
        <para>
            Az öröklődés, vagy más néven származtatás szintén egy fontos fogalom. Az ősosztályból származtathatunk különböző osztályokat, melynek lényege, hogy a származtatott osztályok az 
            ősosztálynak egy konkretizáltabb, specializáltabb formája, de végeredményben ugyanúgy az ősosztály egy tagja. (pl.: a „lakberendezési cikkek” osztályból származtathatjuk a 
            „bútorok” osztályt, amiből szintén származtathatunk egy „székek” osztályt és így tovább…)
        </para>
        <para>
            Nyilván a fent említett példák nagyon egyszerűek és földhöz ragadtak, de véleményem szerint ilyen egyszerű példákkal remekül lehet szemléltetni az OOP lényegét. Ezeknél nyilván 
            vannak szofisztikáltabb, látványosabb és hasznosabb példák is. Nem könnyű elsajátítani ezt a szemléletmódot, mert a való életben nem szoktunk hozzá az ilyen strukturált 
            gondolkodás/szemléletmódhoz. Ennyi bevezető után viszont térjünk át a programokra.
        </para>
        <para>
            <programlisting language="c++"><![CDATA[
// POLARGEN.H
#ifndef POLARGEN__H
#define POLARGEN__H
#include <cstdlib>
#include <cmath>
#include <ctime>

class PolarGen
{
    public: 
        PolarGen()
        {
            nincsTarolt = true;
            std::srand (std::time (NULL));
        }
        ~PolarGen(){}
        double kovetkezo();

    private:
        bool nincsTarolt;
        double tarolt;
};

#endif]]></programlisting>
        </para>
        <para>
            <programlisting language="c++"><![CDATA[
//POLARGEN.CPP
#include "polargen.h"

double PolarGen::kovetkezo()
{
    if (nincsTarolt)
    {
        double u1, u2, v1, v2, w;
        do
        {
            u1 = std::rand () / (RAND_MAX + 1.0);
            u2 = std::rand () / (RAND_MAX + 1.0);
            v1 = 2 * u1 - 1;
            v2 = 2 * u2 - 1;
            w = v1 * v1 + v2 * v2;
        } while (w > 1);
        double r = std::sqrt ((-2 * std::log (w)) / w);
        tarolt = r * v2;
        nincsTarolt = !nincsTarolt;

        return r * v1;
    }
    else
    {
         nincsTarolt = !nincsTarolt;

         return tarolt;
    }
}]]></programlisting>
        </para>
        <para>
            <programlisting language="c++"><![CDATA[
#include <iostream>
#include "polargen.h"
#include "polargen.cpp"

int main(int argc, char **argv)
{
    PolarGen pg;

    for (int i = 0; i < 10; i++)
        std::cout << pg.kovetkezo() << std::endl;
}]]></programlisting>
        </para>
        <para>
            
        </para>       
        <para>
            <mediaobject>
                <imageobject>
                        <imagedata fileref="img/Welch/polarcpp.png" scale="50"/>
                </imageobject>
            </mediaobject>
        </para>     
    </section>        

    <section>
        <title>LZW</title>
        <para>
            Valósítsd meg C-ben az LZW algoritmus fa-építését!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>                
        </para>
    </section>        
        
    <section>
        <title>Fabejárás</title>
        <para>
            Járd be az előző (inorder bejárású) fát pre- és posztorder is!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>                
        </para>
    </section>        
                        
    <section>
        <title>Tag a gyökér</title>
        <para>
            Az LZW algoritmust ültesd át egy C++ osztályba, legyen egy Tree és egy beágyazott Node
            osztálya. A gyökér csomópont legyen kompozícióban a fával!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>                
        </para>
    </section>        
                
    <section>
        <title>Mutató a gyökér</title>
        <para>
            Írd át az előző forrást, hogy a gyökér csomópont ne kompozícióban, csak aggregációban legyen a 
            fával!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link> 
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>  
        </para>
    </section>                     

    <section>
        <title>Mozgató szemantika</title>
        <para>
            Írj az előző programhoz mozgató konstruktort és értékadást, a mozgató konstruktor legyen a mozgató
            értékadásra alapozva!
        </para>
        <para>
            Megoldás videó: <link xlink:href=""></link> 
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>  
        </para>
    </section>     

    <section>
        <title>Minecraft Malmö 5x5x5</title>
        <para>
            <link xlink:href="https://youtu.be/mj3OV5g5guw">https://youtu.be/mj3OV5g5guw</link>
        </para>
    </section>               
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
